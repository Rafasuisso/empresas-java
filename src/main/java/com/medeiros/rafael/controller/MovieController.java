package com.medeiros.rafael.controller;

import com.medeiros.rafael.entity.Movie;
import com.medeiros.rafael.entity.Vote;
import com.medeiros.rafael.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/movies")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @PostMapping("/addMovie")
    public Movie addMovie(@RequestBody  Movie movie){
        return movieService.saveMovie(movie);
    }

    @PostMapping("/addMovies")
    public List<Movie> addmovies(@RequestBody  List<Movie> movies){
        return movieService.saveMovies(movies);
    }

    @GetMapping("/listMovies/")
    public List<Movie> listMovies() {
        return movieService.getMovies();
    }

    @PostMapping("/vote/{movieId, userId, note}")
    public Vote vote(@PathVariable int movieId, @PathVariable int userId, @PathVariable int note) {
        return movieService.voteMovie(movieId, userId, note);
    }

}
