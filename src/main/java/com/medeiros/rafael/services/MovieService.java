package com.medeiros.rafael.services;

import com.medeiros.rafael.entity.Movie;
import com.medeiros.rafael.entity.User;
import com.medeiros.rafael.entity.Vote;
import com.medeiros.rafael.repository.MovieRepository;
import com.medeiros.rafael.repository.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private VoteRepository voteRepository;

    public Movie saveMovie(Movie movie){
        return movieRepository.save(movie);
    }

    public List<Movie> saveMovies(List<Movie> movies){
        return movieRepository.saveAll(movies);
    }

    public List<Movie> getMovies(){
        return movieRepository.findAll();
    }

    public Vote voteMovie(int movieId, int userId, int note){
        Vote vote = new Vote();
        vote.setMovieId(movieId);
        vote.setUserId(userId);
        vote.setNote(note);
        return voteRepository.save(vote);
    }
}
