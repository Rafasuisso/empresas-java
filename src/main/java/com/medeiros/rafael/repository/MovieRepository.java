package com.medeiros.rafael.repository;

import com.medeiros.rafael.entity.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie, Integer> {
}
