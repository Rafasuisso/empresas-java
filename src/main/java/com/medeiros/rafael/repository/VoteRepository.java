package com.medeiros.rafael.repository;

import com.medeiros.rafael.entity.Vote;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VoteRepository extends JpaRepository <Vote, Integer> {

}
