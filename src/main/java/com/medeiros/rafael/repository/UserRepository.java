package com.medeiros.rafael.repository;

import com.medeiros.rafael.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
}
